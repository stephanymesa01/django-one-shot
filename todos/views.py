from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "todos/todo_list_list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    tasks = todo_list.items.all()
    context = {
        'todo_list': todo_list,
        'tasks': tasks,
    }
    return render(request, 'todos/todo_list_detail.html', context)


def create_todo_list(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_todo_list = form.save()
            return redirect('todo_list_detail', id=new_todo_list.id)
    else:
        form = TodoListForm()

    return render(request, 'todos/todo_list_create.html', {'form': form})


def update_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=id)
    else:
        form = TodoListForm(instance=todo_list)

    return render(request, 'todos/todo_list_update.html', {
        'form': form, 'todo_list': todo_list}
        )


def delete_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        todo_list.delete()
        return redirect('todo_list_list')

    return render(request, 'todos/todo_list_delete.html', {
        'todo_list': todo_list}
        )


def todo_item_create(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list_id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }

    return render(request, 'todos/todo_item_create.html', context)


def todo_item_edit(request, id):
    todo_item_edit = get_object_or_404(TodoItem, id=id)

    if request.method == 'POST':
        form = TodoItemForm(request.POST, instance=todo_item_edit)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=id)
    else:
        form = TodoItemForm(instance=todo_item_edit)

    context = {
        "form": form,
    }

    return render(request, 'todos/todo_item_edit.html', context)
